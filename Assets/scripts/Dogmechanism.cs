using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class Dogmechanism : MonoBehaviour
{
    public bool isattcking;
    int state;
    int dest;
    float attacktimer;
    NavMeshAgent agent;
    Animator animator;
    float t;
    RaycastHit hit;
    public AudioSource bake;
    public AudioSource bite;
    public AudioSource salem;
    public ThirdPersonController playr;


    int bk = 0;
    int bt = 0;
    // Start is called before the first frame update
    void Start()
    {
        t = 3;
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        isattcking = false;
        state = Random.Range(0,3);
        dest = Random.Range(0, 3);
        attacktimer = 20f;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position + Quaternion.Euler(0, transform.eulerAngles.y, 0) * Vector3.forward * 0.5f, 0.2f);
    }
    // Update is called once per frame
    void Update()
    {
        if(bk == 1)
        {
            bake.Play();
        }


        if(bt == 1)
        {
            bite.Play();
            salem.Play();
        }

        switch (isattcking)
        {
            case true:
                {
                    bk++;
                    /*agent.speed = 10;*/
                    attack();   
                    break;
                }
            case false: 
                {
                    bk = 0;
                     StateChange();
                     break;
                }
        }
        
    }

    void dogdestination()
    {
        Vector3 destination=new Vector3();
        switch (dest)
        {
            case 0:
                destination = GameObject.Find("point 1").GetComponent<Transform>().position;
                break;
            case 1:
                destination = GameObject.Find("point 2").GetComponent<Transform>().position;
                break;
            case 2:
                destination = GameObject.Find("point 3").GetComponent<Transform>().position;
                break;
        }
        agent.SetDestination(destination);

        if ( (Mathf.Abs(transform.position.x-destination.x)) < 0.13f && (Mathf.Abs(transform.position.z - destination.z)<0.13f) )
        {
            dest = Random.Range(0, 3);
            state = Random.Range(0, 3);
        }
    }

    void StateChange()
    {
        attacktimer -= Time.deltaTime;
        if (attacktimer < 0)
        {
            isattcking = true;
            attacktimer = Random.Range(10, 20);
        }
        switch (state)
        {
            case 0:
                {
                    animator.SetBool("isstanding", true);
                    animator.SetBool("isrunning", false);
                    animator.SetBool("iswalking", false);
                    float timer = Random.Range(1,20);
                    while (timer > 0)
                    {
                        timer -= Time.deltaTime;
                        if (timer < 0)
                        {
                            state = Random.Range(0, 3);
                        }
                    }
                    break;
                }
            case 1:
                {
                    animator.SetBool("isstanding", false);
                    animator.SetBool("isrunning", false);
                    animator.SetBool("iswalking", true);
                    agent.speed = 1;
                    dogdestination();
                    break;
                }
            case 2:
                {
                    animator.SetBool("isstanding", false);
                    animator.SetBool("isrunning", true);
                    animator.SetBool("iswalking", false);
                    agent.speed = 3;
                    dogdestination();
                    break;
                }
        }
    }

    void attack()
    {
        agent.speed = 5;
        int playerLayerMask = 1 << LayerMask.NameToLayer("Player");
        agent.SetDestination(GameObject.Find("player").GetComponent<Transform>().transform.position);
        if (Physics.SphereCast(transform.position, 0.5f, Quaternion.Euler(0, transform.eulerAngles.y, 0) * Vector3.forward, out hit, 0.2f, playerLayerMask))
        {
            agent.isStopped = true;
            if (hit.collider.gameObject.name.Equals("player"))
            {
                bt++;
                animator.SetBool("isattacking", true);
                animator.SetBool("isstanding", false);
                animator.SetBool("isrunning", false);
                animator.SetBool("iswalking", false);
                t -= Time.deltaTime;
                if (t < 0)
                {
                    bt= 0;
                    playr.Na7i7weyej();
                    animator.SetBool("isattacking", false);
                    t = 3;
                    state = Random.Range(0, 3);
                    isattcking = false;
                    agent.isStopped = false;
                }
            }

        }
    }
}
