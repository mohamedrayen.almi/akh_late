using Cinemachine;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class camercontrol : MonoBehaviour
{
    CinemachineVirtualCamera cvc;
    public GameObject lid1;
    public GameObject lid2;
    public Animator anim;
    bool openE=false;
    float timer = 3.5f;
    public int state=0;
    public CinemachineVirtualCamera camerv;
    CinemachineVirtualCamera cam;
    // Start is called before the first frame update

    private void Awake()
    {
        Time.timeScale = 0;
    }
    void Start()
    {
        cam = GetComponent<CinemachineVirtualCamera>();
        cvc=transform.GetComponent<CinemachineVirtualCamera>();
    }

    // Update is called once per frame
    void Update()
    {


        switch(state)
        {
            case 0:
                {
                    if (cvc.m_Lens.FieldOfView >= 1)
                        cvc.m_Lens.FieldOfView -= 0.01f;
                    else
                        state = 1;
                    break;
                }
                

            case 1:
                {
                    lid1.GetComponent<MeshRenderer>().enabled = true;
                    lid2.GetComponent<MeshRenderer>().enabled = true;
                    anim.SetBool("wakeup",true);
                    break;
                }
                

            case 2:
                {
                    lid1.GetComponent<MeshRenderer>().enabled = false;
                    lid2.GetComponent<MeshRenderer>().enabled = false;
                    cam.m_Lens.FieldOfView = 5.5f;
                    GameObject.Find("player").GetComponent<Animator>().SetTrigger("wakupplayer");
                    cam.LookAt= GameObject.Find("headlookat").transform;
                    break;
                }
            case 3:
                {
                    SceneManager.LoadScene(1);
                    break;
                }

        }

    }

    public void AnimationEndEvent()
    {
         state = 2;
            anim.SetBool("wakeup", false);
    }
    public void AnimationEndEventPlayer()
    {
        camerv.GetComponent<camercontrol>().state = 3;

    }



}
